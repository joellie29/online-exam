import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
var MainPage = require('./views/MainPage');

import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

var App = DragDropContext(HTML5Backend)(React.createClass( {


  render:function() {
    return (
      <MainPage className="mainPage"/>
    );
  }
}));

module.exports = App;