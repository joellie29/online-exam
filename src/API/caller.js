'use strict';
var nocache = require('superagent-no-cache');
var request = require('superagent');

// function callPost(url,data,callback){
// request.post(url)
// 	    .set('Content-Type', 'application/json')
// 	    .send(data)
// 	    .end(callback)
//     }

exports.callPost = function (url,data, callback) {
    function start () {
		// request(method, url).then(success, failure);
	  request.post(url)
	    .set('Content-Type', 'application/json')
	    .send(data)
	    .end(callback)
    }

    start();
};
exports.callGet=  function(url,data, callback){
	      console.log("I called");

	function start(){
		request
			.get(url)
			.query(data)
			// .use(nocache)
			.then(callback)
	}
	start();
}
