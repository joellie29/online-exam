import React, { Component } from 'react';
import '../App.css';
var caller = require('../API/caller');
var ListItem = require('./ListItem');
var FavShow = require('./FavShow');
// import { DragDropContext } from 'react-dnd';
// import HTML5Backend from 'react-dnd-html5-backend';
var store = require('store');
var {DropTarget} = require('react-dnd');


const squareTarget = {
  canDrop: function(props,monitor) {
    // console.log(monitor.getItem())
    return monitor.getItem().type=="FavoriteShow";
  },
  drop: function(props,monitor,component) {
    console.log("drop outside");
    console.log(monitor.didDrop())
    if (!monitor.didDrop())
    {
      if (monitor.getItem().type == "AvailableShow")
      {
        // return false;
      }
      else
      {

      window.component.removeItem(monitor.getItem());
      }
    }
    // moveItem(monitor.getItem());
  }

};
function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
  };
}


var MainPage =  DropTarget("ListItem", squareTarget, collect)(React.createClass( {
  removeItem: function(props){
    var id = props.id;
    var r = confirm("Remove " + props.name + " from the favorite list?");
    if (r== true)
    {
      console.log("removethis " +  id);
      var favShowList = this.state.favShowList;
      var newFavShowList = [];
      favShowList.map(function(item,index){
        if (item.id !=id)
        {
          newFavShowList.push(item);
        }
        else{
          console.log("here is removed " + item.id);
        }
      });
      store.set("favShowList",newFavShowList);
      this.setState({favShowList: newFavShowList}, function(){
            this.getList(this.state.availableShowSearch);
      }.bind(this));

    }
    // this.getList(this.state.availableShowSearch);
  },
  getInitialState: function(){
    return {
      availableShowSearch: "",
      favShowFilter: "",
      listAvailableShow: [],
      limiter: 10,
      favShowList:[],
      callLastMinute: false
    };
  },
  componentDidMount: function(){
    window.component = this;

    this.getList("");
    var favShowList = store.get("favShowList") ? store.get("favShowList") : [];
    this.setState({favShowList:favShowList},function(){
      console.log("did mount " + JSON.stringify(this.state.favShowList));
    }.bind(this));
  },
  emitChange: function(){
    // this.setState({"change" : true});
    var query = this.state.availableShowSearch.length ==0 ? "": this.state.availableShowSearch;
    // console.log("this.state.availableShowSearch " + query);
    // this.getList(query);
    var favShowList = store.get("favShowList") ? store.get("favShowList") : [];
    this.setState({favShowList:favShowList},function(){
          this.getList(query);
        }.bind(this));
  },
  getList: function(query){
    // store.clearAll();
    var alreadyFave = [];
    // store.clearAll();
    var favShowList = this.state.favShowList;
    // console.log("favShowList " + favShowList)
    favShowList.map(function(item,index){
      alreadyFave.push(item.id);
    });
    var url = "";
    var params = null;
    var hasQuery = false;
    if (query == null || query.length==0)
    {
      url = "http://api.tvmaze.com/shows";
    }
    else{
      hasQuery = true;
      url = "http://api.tvmaze.com/search/shows";
      params = {q: query};
    }
    console.log("call Last " + this.state.callLastMinute);
    if (this.state.callLastMinute == false)
    {
        this.setState({callLastMinute:true},function(){
          this.callAPI(url,params,hasQuery,alreadyFave);
          setTimeout(function(){
            this.setState({callLastMinute: false});
          }.bind(this),1000);
        }.bind(this));
    }
    else
    {
      setTimeout(function(){
        this.setState({callLastMinute:true},function(){
          this.callAPI(url,params,hasQuery,alreadyFave);
          setTimeout(function(){
            this.setState({callLastMinute: false});
          }.bind(this),1000);
        }.bind(this));
      }.bind(this)
      ,1000);
    }
    // caller.callGet(url,params,function(res,err){
    //     var newList= [];
    //     var data = res.body;
    //     var limiter = data.length;
    //     // limiter = limiter > 10 ? 10 : limiter;
    //     for (var x=0;x<limiter && x<data.length;x++)
    //     {
    //       var id = hasQuery ? data[x].show.id : data[x].id;
    //       // console.log(alreadyFave.indexOf(id));
    //       if (alreadyFave.indexOf(id) >= 0)
    //       {
    //         // limiter ++;
    //         continue;
    //       }
    //       newList.push({
    //         name: hasQuery ? data[x].show.name : data[x].name,
    //         id: id,
    //       });
    //     }
    //     this.setState({listAvailableShow: newList})
    // }.bind(this));
  },
  callAPI: function(url,params,hasQuery,alreadyFave){
    caller.callGet(url,params,function(res,err){
    var newList= [];
    var data = res.body;
    var limiter = data.length;
    // limiter = limiter > 10 ? 10 : limiter;
    for (var x=0;x<limiter && x<data.length;x++)
    {
      var id = hasQuery ? data[x].show.id : data[x].id;
      // console.log(alreadyFave.indexOf(id));
      if (alreadyFave.indexOf(id) >= 0)
      {
        // limiter ++;
        continue;
      }
      newList.push({
        name: hasQuery ? data[x].show.name : data[x].name,
        id: id,
      });
    }
    this.setState({listAvailableShow: newList})
    }.bind(this));

  },
  changeText: function(e){

    var name = e.target.name;
    var val = e.target.value;
    this.setState({[name]: val},function(){
      this.getList(val);
    }.bind(this));
  },
  filterList: function(e){
        var name = e.target.name;
    var val = e.target.value;
    this.setState({[name]: val});
  },
  render:function() {
      var x = this.props.x;
      var y = this.props.y;
      var isOver = this.props.isOver;
      var canDrop = this.props.canDrop;
      var connectDropTarget = this.props.connectDropTarget;

    var listAvailableShow = [];
    var favShowList = this.state.favShowList;
    var favShowHtml = [];
    var favIndex = []
    if (favShowList)
    {
      favShowList.map(function(item,index){
        if (item.name.toUpperCase().includes(this.state.favShowFilter.toUpperCase()))
        {
          favShowHtml.push(<ListItem name ={item.name} key = {item.id} id = {item.id} type="FavoriteShow"/>);
        }
      }.bind(this));

    }
    this.state.listAvailableShow.map(function(item,index){
      listAvailableShow.push(<ListItem name ={item.name} id = {item.id} key ={item.id} type="AvailableShow"/>); 
    }.bind(this));
    return connectDropTarget(
      <div className="App">
        <div className="App-header">
          <h2>Online Exam</h2>
          <h3>Joellie Peralta</h3>
        <div className="App-intro vertical-middle">
          <div className="align-left">
            <input  type="text" value={this.state.availableShowSearch}  name ="availableShowSearch" onChange ={this.changeText}/> 
              <div className = "list">
                <h3>Available shows</h3>
              <div id = "menu" style={{
                position: 'relative',
                width: '200px',
                height: '300px',
                overflow: "auto",
                "background-color":"white",
                border: "2px inset black",

              }}>
              {!listAvailableShow ? <li>No results found</li> : <ul>{listAvailableShow}</ul>}
            </div>
          </div>
        </div>
        <div className = "align-left">
          <input type="text" value={this.state.favShowFilter}  name ="favShowFilter" onChange ={this.filterList}/> 
            <div className = "list">
              <h3>Favorite shows</h3>
                <div id = "menu">
                  <FavShow name = "FavSHowLis">
                    {favShowList ? <ul>{favShowHtml}</ul> : <li>Empty List</li>  }
                  </FavShow>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}));
export function moveItem(props) {
    console.log("drop inside");
    // console.log("props is " + JSON.stringify(props));
    if (props.type=="FavoriteShow")
     return false;
    // console.log(props);
    var r = confirm("Add " + props.name + " to the favorite list?");
    if (r== true)
    {

      var favShowList = store.get("favShowList") ? store.get("favShowList") : [];
      favShowList.push(props);
      // favShowList.push(props);
      store.set("favShowList",favShowList);
      window.component.emitChange();
      // console.log(store.getAll())


  }
}

module.exports = MainPage;