import React, { Component } from 'react';
import '../App.css';
var caller = require('../API/caller');
var {DropTarget} = require('react-dnd');
import { moveItem } from './MainPage';

const squareTarget = {
  canDrop: function(props,monitor) {
    return true;
  },
  drop: function(props,monitor,component) {
    console.log("drop");
    moveItem(monitor.getItem());
  }

};
function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    item: monitor.getItem()
  };
}



var FavShow = DropTarget("ListItem", squareTarget, collect)(React.createClass( {
  componentDidMount: function(){

  },
  render:function() {
      var x = this.props.x;
      var y = this.props.y;
      var isOver = this.props.isOver;
      var canDrop = this.props.canDrop;
      var connectDropTarget = this.props.connectDropTarget;
      var item = this.props.item;
      if (item){
      console.log("render item" + JSON.stringify(item));
        canDrop=item.type =="AvailableShow";
      }
    return connectDropTarget(
          <div className = "" style={{
            position: 'relative',
            width: '200px',
            height: '300px',
            overflow: "auto",
            background: "white",
            border: "2px inset black",
          }}>
            {this.props.children}
          {(isOver && canDrop) &&
            <div style={{
              position: 'absolute',
              top: 0,
              left: 0,
              height: '100%',
              width: '100%',
              zIndex: 1,
              opacity: 0.5,
              backgroundColor: 'yellow',
            }} />
          }

    </div>

    );
  }
}));

module.exports = FavShow;