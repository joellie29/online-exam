import React, { Component } from 'react';
import '../App.css';
var caller = require('../API/caller');
var {DragSource, DragDropContext} = require('react-dnd');


var itemDragSource = {
      beginDrag: function (props) {
      return props;
  },
      // endDrag: function(props,monitor,component){
      //   const item = monitor.getItem();
      //   const dropResult = monitor.getDropResult();
      //   if (dropResult)
      //   {
      //     console.log("dropped " + item.name + " " + JSON.stringify(dropResult));
      //   }
      // }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
};



var ListItem = DragSource("ListItem", itemDragSource, collect)(React.createClass( {

  getInitialState:function(){
    return ({
      name: this.props.name,
      id: this.props.id,
      type : this.props.type
    });
  },
  componentDidMount: function(){

  },
  componenWillReceiveProps: function(nextProps){
    console.log("componenWillReceiveProps " + nextProps);
    // if (nextProps.id !=this.props.id)
    // {
      this.setState({id: nextProps.id,name: nextProps.name, type: nextProps.type});
    // }
  },
  render:function() {
      var connectDragSource = this.props.connectDragSource;
      var isDragging = this.props.isDragging;

    return connectDragSource(
          <div style={{
            opacity: isDragging ? 0.5 : 1,
            fontSize: 12,
            fontWeight: 'bold',
            cursor: 'move',
          }}>
          <ul style={{
            "text-align": "left",
          }}>
            {this.state.name}
          </ul>
    </div>

    );
  }
}));

module.exports = ListItem;